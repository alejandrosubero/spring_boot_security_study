package com.security.estudy.auth;

import com.google.common.collect.Lists;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.security.estudy.security.AplicationUserRole.*;

@Repository("fake")
public class FakeAplicationUserDaoService implements AplicationUserDao {

    private final PasswordEncoder passwordEncoder;

    public FakeAplicationUserDaoService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<AplicationUser> selectAplicationUserByUsername(String username) {
//        return getAplicationUser()
//                .stream()
//                .filter(aplicationUsers->username.equals(aplicationUsers.getUsername()))
//                .findFirst();
        return null;
    }

  //  public List<AplicationUser> getAplicationUser() {
//        List<AplicationUser> aplicationUsers = Lists.newArrayList(
//                new AplicationUser(
//                        "ana", passwordEncoder.encode("123456"), STUDENT.getGrantedAuthorities(),
//                        true, true, true, true
//                ),
//                new AplicationUser(
//                        "tom", passwordEncoder.encode("123456"), ADMINTRAINEE.getGrantedAuthorities(),
//                        true, true, true, true
//                ),
//                new AplicationUser(
//                        "alejandro", passwordEncoder.encode("123456"), ADMIN.getGrantedAuthorities(),
//                        true, true, true, true
//                )
//
//        );
//        return aplicationUsers;
   // }
}
