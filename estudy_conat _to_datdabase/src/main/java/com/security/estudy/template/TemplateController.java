package com.security.estudy.template;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class TemplateController {

    @GetMapping("login")
    public String getloguinView(){
        return "login";
    }

    @GetMapping("course")
    public String getloguinViewsusess(){
        return "course";
    }

}
