package com.security.estudy.auth;

import java.util.Optional;

public interface AplicationUserDao {
    public Optional<AplicationUser> selectAplicationUserByUsername(String username);
}
