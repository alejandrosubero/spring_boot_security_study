package com.security.estudy.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AplicationUserService implements UserDetailsService {
    private final AplicationUserDao aplicationUserDao;

    @Autowired
    public AplicationUserService(@Qualifier("fake") AplicationUserDao aplicationUserDao) {
        this.aplicationUserDao = aplicationUserDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return aplicationUserDao.selectAplicationUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException(String.format("Username %s not found", username)));
    }
}
