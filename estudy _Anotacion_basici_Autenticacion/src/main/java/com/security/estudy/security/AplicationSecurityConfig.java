package com.security.estudy.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.security.estudy.security.AplicationUserRole.*;
import static com.security.estudy.security.ApplicationUserPermission.COUSER_WRITE;
import static com.security.estudy.security.ApplicationUserPermission.STUDENT_WRITE;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // include this, for configurate use anotations
public class AplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AplicationSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()// next secction
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*,/js/*").permitAll()
                .antMatchers("/api/**").hasAnyRole(STUDENT.name(), ADMIN.name())
              //  .antMatchers("/api/**").hasRole(STUDENT.name())  // el orden de la colocacion si importa va de uno en uno
//                .antMatchers(HttpMethod.DELETE,"/management/api/v1/**").hasAuthority(COUSER_WRITE.getPermission())
//                .antMatchers(HttpMethod.POST,"/management/api/v1/**").hasAuthority(COUSER_WRITE.getPermission())
//                .antMatchers(HttpMethod.PUT,"/management/api/v1/**").hasAuthority(COUSER_WRITE.getPermission())
//                .antMatchers(HttpMethod.GET,"/management/api/v1/**").hasAnyRole(ADMIN.name(), ADMINTRAINEE.name())
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }


    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails alejandroUser = User.builder()
                .username("alejandro")
                .password(passwordEncoder.encode("123456"))
              //  .roles(ADMIN.name())
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        UserDetails anaUser = User.builder()
                .username("ana")
                .password(passwordEncoder.encode("123456"))
            //    .roles(STUDENT.name())
                .authorities(STUDENT.getGrantedAuthorities())
                .build();

        UserDetails tomUser = User.builder()
                .username("tom")
                .password(passwordEncoder.encode("123456"))
                // .roles(ADMINTRAINEE.name())
                .authorities(ADMINTRAINEE.getGrantedAuthorities())
                .build();


        return new InMemoryUserDetailsManager(
                alejandroUser, anaUser, tomUser
        );
    }


}
