package com.security.estudy.student;

import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("management/api/v1/students")
public class StudentManagemeController {

    private static final List<Student> STUDENTS = Arrays.asList(
            new Student(1, "james Bond"),
            new Student(2, "maria miller"),
            new Student(3, "Ana Smith"),
            new Student(4, "alejandro")
    );

    @GetMapping
    public List<Student> getAllStudent() {
        return STUDENTS;
    }

    @PostMapping()
    public void registerNewStudent(@RequestBody Student student) {
        System.out.println(student.toString());
    }

    @DeleteMapping("{studentId}")
    public void deliteStudent(@PathVariable("studentId") Integer studentId) {
        System.out.println("DELETE ");
        System.out.println(studentId);
    }

    @PutMapping("{studentId}")
    public Student updateStudent(@PathVariable("studentId") Integer studentId, @RequestBody Student student) {
        System.out.println(String.format("%s %s", studentId, student));
        // System.out.println(String.format("%s %s",STUDENTS.listIterator(studentId),student));
        return student;
    }

}
