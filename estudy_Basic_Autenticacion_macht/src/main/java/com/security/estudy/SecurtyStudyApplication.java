package com.security.estudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurtyStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurtyStudyApplication.class, args);
	}

}
