package com.security.estudy.security;

import com.google.common.collect.Sets;


import java.util.Set;

import static com.security.estudy.security.ApplicationUserPermission.*;

public enum AplicationUserRole {
    STUDENT(Sets.newHashSet()),
    ADMIN(Sets.newHashSet(COURSE_READ, COUSER_WRITE, STUDENT_READ, STUDENT_WRITE));
    private final Set<ApplicationUserPermission> permissions;

    AplicationUserRole(Set<ApplicationUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<ApplicationUserPermission> getPermissions() {
        return permissions;
    }
}
