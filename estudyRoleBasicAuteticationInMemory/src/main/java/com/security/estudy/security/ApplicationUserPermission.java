package com.security.estudy.security;

public enum ApplicationUserPermission {

    STUDENT_READ("studient:read"),
    STUDENT_WRITE("studient:write"),
    COURSE_READ("course:read"),
    COUSER_WRITE("course:write");

    private final String permission;

    ApplicationUserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

}
