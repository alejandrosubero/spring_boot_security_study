package com.security.estudy.security.auth;

import com.security.estudy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AplicationUserService implements UserDetailsService {

    private  final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public AplicationUserService(@Qualifier("user") UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserAuth> user = userRepository.findByUsername(username);
        user.orElseThrow(()-> new UsernameNotFoundException(String.format("Username %s not found", username)));
        user.get().setPassword(passwordEncoder.encode(user.get().getPassword()));// code password
        return user.map(userAuth -> new AplicationUser( userAuth)).get();
    }


}
